#!/bin/sh

nitrogen --restore & disown

picom & disown

# Low battery notifier
~/.config/qtile/scripts/low_bat_notifier.sh & disown

# volumeicon & disown
nm-applet & disown
blueman-applet & disown
copyq & disown
# mpd & disown
syncthing --no-browser & disown

# Turn on numlock
# numlockx & disown

# Remap capslock to backspace
setxkbmap -option caps:backspace & disown

# EndeavourOS app that checks available updates
eos-update-notifier & disown

# start polkit agent from GNOME
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & disown
