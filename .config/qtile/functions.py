from libqtile.lazy import lazy
from libqtile.log_utils import logger

# Display handling
from Xlib import display as xdisplay
from Xlib.ext.randr import Connected as RR_Connected
import re, glob
import warnings


def is_closed_lid(output: str):
  """Determine if laptop lid is closed
    Taken from autorandr: https://github.com/phillipberndt/autorandr/blob/4f010c576/autorandr.py#L99-L108"""
  if not re.match(r'(eDP(-?[0-9]\+)*|LVDS(-?[0-9]\+)*)', output):
    return False
  lids = glob.glob("/proc/acpi/button/lid/*/state")
  if len(lids) == 1:
    state_file = lids[0]
    with open(state_file) as f:
      content = f.read()
      return "close" in content
  return False


def get_num_monitors():
  """Get the number of monitors activated on the computer"""
  num_monitors = 0
  try:
    display = xdisplay.Display()
    screen = display.screen()
    resources = screen.root.xrandr_get_screen_resources()._data

    for output in resources['outputs']:
      monitor = display.xrandr_get_output_info(
          output, resources['config_timestamp'])._data
      if monitor['connection'] == RR_Connected and not is_closed_lid(
          monitor['name']):
        num_monitors += 1
  except Exception:
    return 1  #always return at least 1 monitor
  else:
    return num_monitors


def get_monitor_resolutions() -> list[tuple]:
  """Resolutions of monitors"""
  resolutions = []
  try:
    display = xdisplay.Display()
    screen = display.screen()
    resources = screen.root.xrandr_get_screen_resources()._data

    for output in resources['outputs']:
      monitor = display.xrandr_get_output_info(
          output, resources['config_timestamp'])._data
      if monitor['connection'] == RR_Connected and not is_closed_lid(
          monitor['name']) and monitor['crtc']:
        crtc = display.xrandr_get_crtc_info(monitor['crtc'],
                                            resources['config_timestamp'])._data
        resolutions.append((crtc['width'], crtc['height']))
  except Exception:
    logger.error('Failed to get monitor resolutions', exc_info=True)
    return [(0, 0)]
  else:
    return resolutions


def get_XScreen_resolution():
  """Get resolution of XScreen (not monitors)"""
  try:
    display = xdisplay.Display()
    screen = display.screen()
  except Exception:
    return (0, 0)
  else:
    return (screen.width_in_pixels, screen.height_in_pixels)


def ungrab_chord():
  """Shortened version for ungrabing keychords"""

  @lazy.function
  def _ungrab_chord(qtile):
    warnings.warn('Replaced by `lazy.ungrab_chord()`', DeprecationWarning)
    qtile.ungrab_chord()

  return _ungrab_chord


def warp_cursor_here_win(win):
  if win is not None:
    win.window.warp_pointer(round(win.width * 0.8), round(win.height * 0.8))


def warp_cursor_here():
  '''Warp the cursor to the currently focused window'''

  @lazy.function
  def _warp_cursor_here(qtile):
    win = qtile.current_window
    warp_cursor_here_win(win)

  return _warp_cursor_here


def view_group(name):
  '''Smarter group view switching
    Display group on current screen if not otherwise displayed. If group is
    already displayed, switch focus to that screen
    Modified from: https://gist.github.com/TauPan/9c09bd9defc5ac3c9e06#file-config-py-L86-L103
    '''

  @lazy.function
  def _view_group(qtile):

    group = qtile.groups_map[name]
    if group != qtile.current_group:
      if group.screen:
        qtile.cmd_to_screen(group.screen.index)
        warp_cursor_here_win(qtile.current_window)
      else:
        group.cmd_toscreen()
        warp_cursor_here_win(qtile.current_window)

  return _view_group
