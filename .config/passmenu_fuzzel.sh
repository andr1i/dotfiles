#!/usr/bin/env bash

shopt -s nullglob globstar

typeit=0
if [[ $1 == "--type" ]]; then
  typeit=1
  shift
fi

if [[ -n $WAYLAND_DISPLAY ]]; then
  fuzzel_command="pkill fuzzel || fuzzel --icon-theme=candy-icons --background-color=1D1D27ee --text-color=cdd6f4ff --match-color=AC16C7FF --border-width=3 --border-radius=10 --border-color=8701E7FF --selection-color=585b70ff --selection-text-color=cdd6f4ff --selection-match-color=AC16C7FF --font='Torus'  --prompt='   ' --dmenu"
  xdotool="ydotool type --file -"
elif [[ -n $DISPLAY ]]; then
  fuzzel_command="pkill fuzzel || fuzzel --icon-theme=candy-icons --background-color=1D1D27ee --text-color=cdd6f4ff --match-color=AC16C7FF --border-width=3 --border-radius=10 --border-color=8701E7FF --selection-color=585b70ff --selection-text-color=cdd6f4ff --selection-match-color=AC16C7FF --font='Torus'  --prompt='   ' --dmenu"
  xdotool="xdotool type --clearmodifiers --file -"
else
  echo "Error: No Wayland or X11 display detected" >&2
  exit 1
fi

prefix=${PASSWORD_STORE_DIR-~/.password-store}
password_files=( "$prefix"/**/*.gpg )
password_files=( "${password_files[@]#"$prefix"/}" )
password_files=( "${password_files[@]%.gpg}" )

# Using the specified fuzzel command here
password=$(printf '%s\n' "${password_files[@]}" | eval "$fuzzel_command")

[[ -n $password ]] || exit

if [[ $typeit -eq 0 ]]; then
  pass show -c "$password" 2>/dev/null
else
  pass show "$password" | { IFS= read -r pass; printf %s "$pass"; } | $xdotool
fi

