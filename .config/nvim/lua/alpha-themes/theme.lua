local status_ok, alpha = pcall(require, "alpha")
if not status_ok then
 return
end

local dashboard = require("alpha.themes.dashboard")
dashboard.section.header.val = {

[[    /\_/\           ___]],
[[   = o_o =_______    \ \  ]],
[[    __^      __(  \.__) )]],
[[(@)<_____>__(_____)____/]],

}

 dashboard.section.buttons.val = {
}


dashboard.section.footer.val = {
"Хліб",
"Куряче філе",
"Молоко",
"Води 6л",
"Кетчук",
"Яєць 20 штук",
"Сир хз який",
"Цукерок але тільки не рошен",
}

dashboard.opts.opts.noautocmd = true
alpha.setup(dashboard.opts)
