#!/usr/bin/env bash

# Check for Wayland or X11 and set up fuzzel and xdotool/ydotool accordingly
if [[ -n $WAYLAND_DISPLAY ]]; then
  fuzzel_command="fuzzel --no-fuzzy --icon-theme=candy-icons --background-color=1D1D27ee --text-color=cdd6f4ff --match-color=AC16C7FF --border-width=3 --border-radius=10 --border-color=8701E7FF --selection-color=585b70ff --selection-text-color=cdd6f4ff --selection-match-color=AC16C7FF --font='Torus'  --prompt='🔆:' --dmenu"
elif [[ -n $DISPLAY ]]; then
  fuzzel_command="fuzzel --no-fuzzy --icon-theme=candy-icons --background-color=1D1D27ee --text-color=cdd6f4ff --match-color=AC16C7FF --border-width=3 --border-radius=10 --border-color=8701E7FF --selection-color=585b70ff --selection-text-color=cdd6f4ff --selection-match-color=AC16C7FF --font='Torus'  --prompt='🔆:' --dmenu"
else
  echo "Error: No Wayland or X11 display detected" >&2
  exit 1
fi

# Define brightness levels
brightness_levels=(100 90 80 70 60 50 40 30 20 15 10 5)

# Using the specified fuzzel command to choose a brightness level
selected_brightness=$(printf '%s\n' "${brightness_levels[@]}" | eval "$fuzzel_command")

[[ -n $selected_brightness ]] || exit

# Set the brightness using the light command
light -S "$selected_brightness"

